import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowKittiesComponent } from './show-kitties/show-kitties.component';
import { AddKittiesComponent } from './add-kitties/add-kitties.component';

const routes: Routes = [
    {path:"add-kitties", component: AddKittiesComponent},
    {path:"show-kitties", component: ShowKittiesComponent}
 ];

  @NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { } export const
RoutingComponent = [AddKittiesComponent, ShowKittiesComponent];

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Котики';
  breeds = [
  {id: 0, name: "Бенгалский", image: "assets/cats/bengali.jpg"},
  {id: 1, name: "Британец", image: "assets/cats/brit.jpg"},
  {id: 2, name: "Мейнкун", image: "assets/cats/maincoon.jpg"},
  {id: 3, name: "Персидский", image: "assets/cats/persian.jpg"},
  {id: 4, name: "Сиамский", image: "assets/cats/siamese.jpg"},
  ];

  cats: any[] = [];

  onNewKitty(kitty: any) {
  console.log(kitty);
  this.cats = [];
  this.cats.push(kitty);
  }
}

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-show-kitties',
  templateUrl: './show-kitties.component.html',
  styleUrls: ['./show-kitties.component.css']
})
export class ShowKittiesComponent implements OnInit {
  @Input() breeds: any[] = [];
  @Input() cats: any[] = [];


  constructor() { }

  ngOnInit(): void {
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowKittiesComponent } from './show-kitties.component';

describe('ShowKittiesComponent', () => {
  let component: ShowKittiesComponent;
  let fixture: ComponentFixture<ShowKittiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowKittiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowKittiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddKittiesComponent } from './add-kitties.component';

describe('AddKittiesComponent', () => {
  let component: AddKittiesComponent;
  let fixture: ComponentFixture<AddKittiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddKittiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddKittiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

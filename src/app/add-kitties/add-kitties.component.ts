import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add-kitties',
  templateUrl: './add-kitties.component.html',
  styleUrls: ['./add-kitties.component.css']
})
export class AddKittiesComponent implements OnInit {
  @Input() breeds: any[] = [];
  @Output() newKitty = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  console.log(history.state)
  if(typeof history.state.breeds != "undefined")
    this.breeds = history.state.breeds;
  }

  onFormSubmit(data: any){
    this.newKitty.emit(data);
  }
}
